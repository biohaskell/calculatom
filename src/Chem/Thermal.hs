module Chem.Thermal
  ( ThermalProfile(..)
  )
where

import           Numeric.Units.Dimensional.Prelude
import           RIO

data ThermalProfile = ThermalProfile {
  boilingPoint :: ThermodynamicTemperature Double
, meltingPoint :: ThermodynamicTemperature Double
, specificHeat :: SpecificHeatCapacity Double
} deriving (Eq, Show)

