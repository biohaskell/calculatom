module Chem.Elements
  ( Element(..)
  , ElementSymbol(..)
  , Isotope(..)
  , NeutronNumber
  , Nucleons
  , ProtonNumber
  , elements
  , hydrogen
  )
where

import           Chem.Thermal
import           Numeric.Units.Dimensional.Prelude
import           Numeric.Units.Dimensional.NonSI
                                                ( dalton )
import           RIO                     hiding ( (*)
                                                , (/)
                                                )
import qualified RIO.HashMap                   as HM

data ElementSymbol =
        H  |                                                                                 He |
        Li | Be |                                                   B  | C  | N  | O  | F  | Ne |
        Na | Mg |                                                   Al | Si | P  | S  | Cl | Ar |
        K  | Ca | Sc | Ti | V  | Cr | Mn | Fe | Co | Ni | Cu | Zn | Ga | Ge | As | Se | Br | Kr |
        Rb | Sr | Y  | Zr | Nb | Mo | Tc | Ru | Rh | Pd | Ag | Cd | In | Sn | Sb | Te | I  | Xe |
        Cs | Ba | La |

                  Ce | Pr | Nd | Sm | Eu | Gd | Tb | Dy | Ho | Er | Tm | Yb | Lu

                     deriving (Show, Read, Eq, Ord, Enum, Bounded, Generic)

instance Hashable ElementSymbol

type ProtonNumber  = Int
type NeutronNumber = Int
type Nucleons = (ProtonNumber, NeutronNumber)

data Element = Element {
  atomicNumber      :: Int
, atomicName        :: Text
, atomicRadius      :: Length Double
, covalentRadius    :: Length Double
, thermalProfile    :: ThermalProfile
, vanDerWaalsRadius :: Maybe (Length Double)
, isotopes          :: [Isotope]
} deriving (Eq, Show)

data Isotope = Isotope {
  nucleons          :: Nucleons
, isotopicMass      :: Mass Double
, isotopicAbundance :: Dimensionless Double
} deriving (Eq, Show)

hydrogen :: Element
hydrogen = Element
  { atomicNumber      = 1
  , atomicName        = "hydrogen"
  , atomicRadius      = (53 *~ pico metre)
  , covalentRadius    = (31 *~ pico metre)
  , thermalProfile    = ThermalProfile
    { boilingPoint = 20.28 *~ kelvin
    , meltingPoint = 14.01 *~ kelvin
    , specificHeat = 14300 *~ (joule / (kilo gram * kelvin))
    }
  , vanDerWaalsRadius = Just (109 *~ pico metre)
  , isotopes = [ Isotope (1, 0) (1.00782503223 *~ dalton) (0.999885 *~ one)
               , Isotope (1, 1) (2.01410177812 *~ dalton) (0.000115 *~ one)
               ]
  }

elements :: HashMap ElementSymbol Element
elements = HM.fromList [(H, hydrogen)]
